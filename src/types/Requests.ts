
export interface IOrdering {
  column: string;
}

export interface IPaging {
  perPage?: number;
  total?: number;
  page: number;
}

export interface IPaginatedRequest {
  ordering?: IOrdering;
  paging?: IPaging;
}

export interface IPaginatedResult<T> {
  data: T[];
  paging: IPaging;
  ordering: IOrdering;
}
