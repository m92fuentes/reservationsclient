import { AbstractControl, ValidatorFn } from '@angular/forms';

function validateDate(date: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const valid = date instanceof Date || date.test(control.value);
    return valid ? {forbiddenName: {value: control.value}} : null;
  };
}

export default validateDate;
