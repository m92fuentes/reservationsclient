import { RESERVATIONS_BY_CONTACT_ROUTE, RESERVATIONS_ROUTE } from '../modules/reservations/constants';
import {
  RESERVATION_CREATION_FROM_CONTACT,
  RESERVATION_CREATION_ROUTE,
  RESERVATION_EDITION_ROUTE
} from '../modules/reservation-creation/constants';
import {CONTACTS_LIST_ROUTE} from '../modules/contacts';
import {CONTACTS_CREATE_ROUTE, CONTACTS_EDIT_ROUTE} from '../modules/contacts-create';

export function isOnReservationPath(url): boolean {
  return `/${RESERVATIONS_ROUTE}` === url;
}

export function isOnReservationCreationPath(url): boolean {
  return `/${RESERVATION_CREATION_ROUTE}` === url;
}

export function isOnReservationEditionPath(url): boolean {
  return compareUrlWithId(RESERVATION_EDITION_ROUTE, url);
}

export function isOnReservationByContactPath(url): boolean {
  return compareUrlWithId(RESERVATIONS_BY_CONTACT_ROUTE, url);
}

export function isOnReservationCreationFromContactPath(url): boolean {
  return compareUrlWithId(RESERVATION_CREATION_FROM_CONTACT, url);
}

export function isOnContactsPath(url): boolean {
  return `/${CONTACTS_LIST_ROUTE}` === url;
}

export function isOnContactEditionPath(url): boolean {
  return compareUrlWithId(CONTACTS_EDIT_ROUTE, url);
}

export function isOnContactCreationPath(url): boolean {
  return `/${CONTACTS_CREATE_ROUTE}` === url;
}

function compareUrlWithId(urlConstant, url): boolean {
  const segments = urlConstant.split('/').slice(0, -1);
  const urlSegments = url.slice(1).split('/').slice(0, -1);

  return compareArrays(segments, urlSegments);
}

export function getIdFromUrl(url: string): string {
  return url.split('/').slice(-1)[0];
}

export function compareArrays(arr1, arr2): boolean {
  if (arr1.length !== arr2.length){
    return false;
  }
  return arr1.every((v, i) => arr2[i] === v);
}
