import { Component, Input, OnInit } from '@angular/core';
import IReservation from '../../../types/IReservation';
import { ReservationsProviderService } from '../../../app/services/reservations-provider.service';
import { Router } from '@angular/router';
import { RESERVATION_EDITION_ROUTE } from '../../reservation-creation';
import {IsMobileService} from '../../../app/services/is-mobile.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  @Input() reservation: IReservation;
  isMobile = false;
  constructor(
    private reservationProvider: ReservationsProviderService,
    private router: Router,
    private isMobileService: IsMobileService,
  ) {
    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  ngOnInit(): void {
  }

  rankingClicked(ranking): void {
    this.reservationProvider.updateReservation({
      ...this.reservation,
      ranking,
    });
  }

  favoriteClicked(): void {
    if (!this.reservation.favorite) {
      this.reservationProvider.updateReservation({
        ...this.reservation,
        favorite: true,
      });
    }
  }

  editClicked(): void {
    this.router.navigateByUrl(RESERVATION_EDITION_ROUTE.replace(':id', this.reservation.reservationId.toString()));
  }

  remove(): void {
    this.reservationProvider.removeReservation(this.reservation.reservationId);
  }
}
