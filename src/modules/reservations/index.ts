import { RESERVATIONS_ROUTE, RESERVATIONS_BY_CONTACT_ROUTE } from './constants';
import { ReservationListComponent } from './reservation-list/reservation-list.component';

export {
  RESERVATIONS_ROUTE,
  ReservationListComponent,
  RESERVATIONS_BY_CONTACT_ROUTE,
};
