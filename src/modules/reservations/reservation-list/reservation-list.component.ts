import {Component, OnDestroy, OnInit} from '@angular/core';
import Api from '../../../app/services/Api/index.js.js';
import {Router} from '@angular/router';
import { IPaging } from '../../../types/Requests';
import { ReservationsProviderService } from '../../../app/services/reservations-provider.service';
import { Subscription } from 'rxjs';
import { CONTACTS_LIST_ROUTE } from '../../contacts';
import {RESERVATION_CREATION_FROM_CONTACT, RESERVATION_CREATION_ROUTE} from '../../reservation-creation/constants';
import {RESERVATIONS_ROUTE} from '../constants';
import {IsMobileService} from '../../../app/services/is-mobile.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit, OnDestroy {

  reservations = [];
  pagination: IPaging = { page: 1 };

  subscription: Subscription;
  showNoContactFoundMessage = false;
  contact;
  isMobile = false;

  constructor(
    private reservationsProvider: ReservationsProviderService,
    private api: Api,
    private router: Router,
    private isMobileService: IsMobileService,
  ) {
    this.showErrorAndRedirect = this.showErrorAndRedirect.bind(this);
    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  ngOnInit(): void {
    this.subscribeToReservationObserver(this.reservationsProvider.paginatedReservationSubject);

    this.reservationsProvider.contactNotFoundSubject.subscribe(
      v => v && this.showErrorAndRedirect(),
    );
    this.reservationsProvider.byContactSubject.subscribe(
      contact => this.contact = contact
    );
  }

  showErrorAndRedirect(): void {
    this.showNoContactFoundMessage = true;
    setTimeout(() => {
      this.router.navigateByUrl(RESERVATIONS_ROUTE);
    }, 3000);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  subscribeToReservationObserver(obs): void {
    this.subscription = obs.subscribe(
      resp => {
        this.reservations = resp.data;
        this.pagination = resp.paging;
      }
    );
  }

  createReservation(): void {
    if (this.contact){
      this.router.navigateByUrl(RESERVATION_CREATION_FROM_CONTACT.replace(':id', this.contact.contactId));
    }
    else {
      this.router.navigateByUrl(RESERVATION_CREATION_ROUTE);
    }
  }

  sortChanged(column: string): void {
    this.reservationsProvider.changeOrdering( { column });
  }

  pageClicked(page): void {
    this.reservationsProvider.changePage(page);
  }

  listContactsClicked(): void {
    this.router.navigateByUrl(CONTACTS_LIST_ROUTE);
  }

  goToAllReservations(): void {
    this.router.navigateByUrl(RESERVATIONS_ROUTE);
  }
}
