import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { ReservationComponent } from './reservation/reservation.component';
import { StarsComponent } from './stars/stars.component';
import {AngularSvgIconModule} from 'angular-svg-icon';

@NgModule({
  declarations: [ReservationListComponent, ReservationComponent, StarsComponent],
  imports: [
    CommonModule,
    SharedComponentsModule,
    AngularSvgIconModule,
  ]
})
export class ReservationsModule { }
