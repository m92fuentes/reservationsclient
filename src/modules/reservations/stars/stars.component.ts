import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {

  @Input() stars = 0;
  @Output() rankingClicked = new EventEmitter();
  totalStars = Array(5).fill(0);

  constructor() { }

  ngOnInit(): void {
  }

}
