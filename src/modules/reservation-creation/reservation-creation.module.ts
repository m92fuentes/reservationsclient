import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationCreationComponent } from './reservation-creation/reservation-creation.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ReservationCreationComponent, RichTextEditorComponent],
  imports: [
    CommonModule,
    SharedComponentsModule,
    AngularSvgIconModule,
    EditorModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' }
  ]
})
export class ReservationCreationModule { }
