import { RESERVATION_CREATION_ROUTE, RESERVATION_EDITION_ROUTE, RESERVATION_CREATION_FROM_CONTACT } from './constants';
import { ReservationCreationComponent } from './reservation-creation/reservation-creation.component';

export {
  ReservationCreationComponent,
  RESERVATION_EDITION_ROUTE,
  RESERVATION_CREATION_ROUTE,
  RESERVATION_CREATION_FROM_CONTACT,
};
