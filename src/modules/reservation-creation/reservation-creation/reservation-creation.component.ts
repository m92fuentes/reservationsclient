import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval, Subject} from 'rxjs';
import { debounce } from 'rxjs/operators';
import Api from '../../../app/services/Api/index.js';
import validateDate from '../../../shared/validators/validateDate';
import IReservation from '../../../types/IReservation';
import {RESERVATIONS_BY_CONTACT_ROUTE, RESERVATIONS_ROUTE} from '../../reservations/constants';
import {isOnReservationCreationFromContactPath, isOnReservationCreationPath} from '../../../shared/helpers';
import {IsMobileService} from '../../../app/services/is-mobile.service';

@Component({
  selector: 'app-reservation-creation',
  templateUrl: './reservation-creation.component.html',
  styleUrls: ['./reservation-creation.component.scss']
})
export class ReservationCreationComponent implements OnInit {
  nameFormControl = new FormControl('', Validators.required);
  reservationForm = new FormGroup({
    contact: new FormGroup({
      name: this.nameFormControl,
      contactType: new FormControl('', Validators.required),
      birthDate: new FormControl('', [
        Validators.required,
        validateDate(/\d{1,2}\/\d{1,2}\/\d{1,2}/)]
      ),
      phoneNumber: new FormControl(''),
    }),
    reservation: new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl(''),
    }),
  });

  nameChangeSubject = new Subject();
  contactFetched;
  reservationId;
  showNoReservationFound = false;
  isFetching = false;

  isMobile = false;

  constructor(
    private router: Router,
    private api: Api,
    private activateRoute: ActivatedRoute,
    private isMobileService: IsMobileService,
  ) {
    this.onNameChange = this.onNameChange.bind(this);
    this.clearContact = this.clearContact.bind(this);
    this.sendReservation = this.sendReservation.bind(this);
    this.fillFormWithReservation = this.fillFormWithReservation.bind(this);
    this.showMessageAndRedirect = this.showMessageAndRedirect.bind(this);
    this.goToReservations = this.goToReservations.bind(this);

    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  ngOnInit(): void {
    this.nameFormControl.valueChanges.subscribe(
      v => this.nameChangeSubject.next(v)
    );
    // Only triggers onNameChange after 500 milliseconds had passed since last key pressed
    this.nameChangeSubject
      .pipe(debounce(() => interval(500)))
      .subscribe(this.onNameChange);

    const { snapshot } = this.activateRoute;

    if (snapshot.url[1].path === 'edit') {
      this.reservationId = +this.activateRoute.snapshot.params.id;
      this.isFetching = true;
      if (this.isEditing()) {
        this.api.getReservationById(this.reservationId).subscribe(
          this.fillFormWithReservation,
          this.showMessageAndRedirect,
        );
      }
    }

    if (this.isOnCreationByContact()) {
      this.isFetching = true;
      const contactId = +this.activateRoute.snapshot.params.id;
      this.api.getContactById(contactId).subscribe(
        contact => {
          this.isFetching = false;
          this.contactFetched = contact;
          this.reservationForm.patchValue({ contact });
        },
        this.showMessageAndRedirect,
      );
    }
  }

  isOnCreationByContact(): boolean {
    return isOnReservationCreationFromContactPath(this.router.url);
  }

  showMessageAndRedirect(): void {
    this.showNoReservationFound = true;
    setTimeout(() => this.goToReservations(), 3000);
  }

  fillFormWithReservation(reservation: IReservation): void {
    this.isFetching = false;
    this.contactFetched = reservation.contact;
    this.reservationForm.patchValue({
      reservation,
      contact: reservation.contact,
    });
  }

  isEditing(): boolean {
    return typeof this.reservationId === 'number';
  }

  onNameChange(v): void{
    this.api.getContactByName(v).subscribe(
      contact => {
        delete contact.name;
        this.contactFetched = contact;
        this.reservationForm.patchValue({ contact });
      },
      this.clearContact
    );
  }

  clearContact(): void {
    this.contactFetched = null;
    this.reservationForm.patchValue({
      contact: {
        phoneNumber: '',
        birthDate: '',
        contactType: '',
      }
    });
  }

  goToReservations(): void {
    if (this.isOnCreationByContact()){
      this.router.navigateByUrl(RESERVATIONS_BY_CONTACT_ROUTE.replace(':id', this.contactFetched.contactId));
    }
    else {
      this.router.navigateByUrl(RESERVATIONS_ROUTE);
    }
  }

  submitClick(): void {
    const { contact, reservation } = this.reservationForm.value;
    if (this.submitDisabled()){
      return;
    }
    if (this.isEditing()){
      this.api.updateReservation({
        ...reservation,
        reservationId: this.reservationId,
        contactId: this.contactFetched.contactId,
      }).subscribe(this.goToReservations);
    }
    else if (this.isOnCreationByContact()){
      this.sendReservation(this.contactFetched);
    }
    else if (this.contactFetched) {
      this.api.updateContact({
        ...contact,
        contactId: this.contactFetched.contactId
      }).subscribe(this.sendReservation);
    }
    else {
      this.api.createContact(contact).subscribe(this.sendReservation);
    }
  }

  submitDisabled(): boolean {
    return this.reservationForm.invalid || this.isFetching || this.showNoReservationFound;
  }

  sendReservation(contact): void {
    const { reservation } = this.reservationForm.value;
    this.api.createReservation({
      ...reservation,
      contactId: contact.contactId
    }).subscribe(this.goToReservations);
  }

  isCreating(): boolean {
    return isOnReservationCreationPath(this.router.url) || isOnReservationCreationFromContactPath(this.router.url);
  }
}
