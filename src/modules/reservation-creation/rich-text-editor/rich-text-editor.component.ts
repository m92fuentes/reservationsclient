import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BreakpointObserver } from '@angular/cdk/layout';
import { mobileMedia } from '../../../app/constant';

const mediumSize = '(max-width: 1170px)';
const minSize = mobileMedia;

const bigToolbar = ' undo redo | bold italic backcolor  | formatselect |paste pastetext searchreplace | alignleft aligncenter alignright alignjustify |bullist numlist outdent indent | removeformat | help | image media';

const mediumToolbar = [
  'undo redo | bold italic backcolor  | formatselect | alignleft aligncenter alignright alignjustify',
  'bullist numlist outdent indent |paste pastetext searchreplace  | removeformat | help | image media',
];

const shortToolbar = [
  ' bold italic backcolor  | formatselect ',
  'paste pastetext searchreplace | undo redo | alignleft aligncenter alignright alignjustify',
  'bullist numlist outdent indent | removeformat | help | image media',
];

@Component({
  selector: 'app-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RichTextEditorComponent),
      multi: true
    },
  ]
})
export class RichTextEditorComponent implements OnInit, ControlValueAccessor {

  @Input() tabIndex;
  toolbar: string | string[] = bigToolbar;
  height = 500;
  show = true;
  private pValue;
  get value(): string{
    return this.pValue;
  }
  set value(v) {
    this.pValue = v;

    if (this.pushChanges){
      this.pushChanges(v);
    }
  }
  private layoutChanges;
  private pushChanges;
  constructor(public breakpointObserver: BreakpointObserver) {
    this.listenToChanges = this.listenToChanges.bind(this);
  }

  ngOnInit(): void {

    this.layoutChanges = this.breakpointObserver.observe([
      mediumSize,
      minSize,
    ]);
    this.layoutChanges.subscribe(this.listenToChanges);
  }

  listenToChanges(match): void {
    if (match.matches) {
      if (match.breakpoints[minSize]) {
        this.toolbar = shortToolbar;
        this.height = 300;
      }
      else if (match.breakpoints[mediumSize]){
        this.toolbar = mediumToolbar;
        this.height = 400;
      }
    }
    else {
      this.toolbar = bigToolbar;
      this.height = 500;
    }

    this.reInitTinyMce();
  }

  reInitTinyMce(): void {
    this.show = false;
    setTimeout(() => this.show = true, 20);
  }

  registerOnTouched(fn: any): void {}
  registerOnChange(fn: any): void {
    this.pushChanges = fn;
  }
  writeValue(val: any): void {
    this.value = val;
  }

}
