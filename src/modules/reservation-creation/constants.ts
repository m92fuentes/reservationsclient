export const RESERVATION_CREATION_ROUTE = 'reservations/create';
export const RESERVATION_EDITION_ROUTE = 'reservations/edit/:id';
export const RESERVATION_CREATION_FROM_CONTACT = 'reservations/create/fromContact/:id';
