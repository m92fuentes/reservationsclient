import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AngularSvgIconModule} from 'angular-svg-icon';

@NgModule({
  declarations: [ContactCreateComponent],
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
  ]
})
export class ContactsCreateModule { }
