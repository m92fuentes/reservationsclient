import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { CONTACTS_LIST_ROUTE } from '../../contacts';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import validateDate from '../../../shared/validators/validateDate';
import Api from '../../../app/services/Api/index.js';
import IContact from '../../../types/IContact';
import {IsMobileService} from '../../../app/services/is-mobile.service';
import {isOnContactCreationPath} from '../../../shared/helpers';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.scss']
})
export class ContactCreateComponent implements OnInit {

  nameFormControl = new FormControl('', Validators.required);
  reservationForm = new FormGroup({
    name: this.nameFormControl,
    contactType: new FormControl('', Validators.required),
    birthDate: new FormControl('', [
      Validators.required,
      validateDate(/\d{1,2}\/\d{1,2}\/\d{1,2}/)]
    ),
    phoneNumber: new FormControl(''),
  });

  contactId;
  showNoContactFound = false;
  showDuplicatedNameError = false;
  fetching = false;

  isMobile = false;

  constructor(
    private router: Router,
    private api: Api,
    private activateRoute: ActivatedRoute,
    private isMobileService: IsMobileService,
  ) {
    this.fillForm = this.fillForm.bind(this);
    this.showMessageAndRedirect = this.showMessageAndRedirect.bind(this);
    this.goToContactsList = this.goToContactsList.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  ngOnInit(): void {
    // Clear duplicated error message on name change
    this.nameFormControl.valueChanges.subscribe(
      () => this.showDuplicatedNameError = false
    );

    const {snapshot} = this.activateRoute;

    if (snapshot.url[1].path === 'edit') {
      this.contactId = +this.activateRoute.snapshot.params.id;
      this.fetching = true;
      this.api.getContactById(this.contactId).subscribe({
        next: this.fillForm,
        error: this.showMessageAndRedirect,
      });
    }

    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  fillForm(contact: IContact): void {
    this.fetching = false;
    this.reservationForm.patchValue(contact);
  }

  showMessageAndRedirect(): void {
    this.showNoContactFound = true;
    setTimeout(() => this.goToContactsList, 3000);
  }

  isEditing(): boolean {
    return typeof this.contactId === 'number';
  }

  submitClick(): void {
    if (this.isEditing()) {
      this.api.updateContact({
        ...this.reservationForm.value,
        contactId: this.contactId
      }).subscribe({
        next: this.goToContactsList,
        error: this.handleError,
      });
    }
    else {
      this.api.createContact(this.reservationForm.value).subscribe({
        next: this.goToContactsList,
        error: this.handleError,
      });
    }
  }

  handleError(resp): void {
    if (resp.status === 409 && resp.error.startsWith('Already exists a contact with name')){
      this.showDuplicatedNameError = true;
    }
  }

  goToContactsList(): void {
    this.router.navigateByUrl(CONTACTS_LIST_ROUTE);
  }

  isCreating(): boolean {
    return isOnContactCreationPath(this.router.url);
  }
}
