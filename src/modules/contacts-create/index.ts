import { CONTACTS_CREATE_ROUTE, CONTACTS_EDIT_ROUTE } from './constants';
import { ContactCreateComponent } from './contact-create/contact-create.component';

export {
  CONTACTS_CREATE_ROUTE,
  ContactCreateComponent,
  CONTACTS_EDIT_ROUTE,
};
