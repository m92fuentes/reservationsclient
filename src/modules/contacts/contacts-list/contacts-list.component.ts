import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RESERVATIONS_BY_CONTACT_ROUTE, RESERVATIONS_ROUTE} from '../../reservations/constants';
import { ContactsProviderService } from '../../../app/services/contacts-provider.service';
import { IOrdering, IPaging } from '../../../types/Requests';
import { Subscription } from 'rxjs';
import IContact from '../../../types/IContact';
import {CONTACTS_CREATE_ROUTE, CONTACTS_EDIT_ROUTE} from '../../contacts-create/constants';
import {IsMobileService} from '../../../app/services/is-mobile.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit, OnDestroy {

  contacts: IContact[];
  pagination: IPaging = { page: 1 };
  ordering: IOrdering;

  private subscription: Subscription;
  isMobile = false;

  constructor(
    private router: Router,
    private contactsProvider: ContactsProviderService,
    private isMobileService: IsMobileService,
  ) { }

  ngOnInit(): void {
    this.subscribeToReservationObserver(this.contactsProvider.paginatedContactsSubject);
    this.contactsProvider.fetchContacts();

    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscribeToReservationObserver(obs): void {
    this.subscription = obs.subscribe(
      resp => {
        this.contacts = resp.data;
        this.pagination = resp.paging;
        this.ordering = resp.ordering;
      }
    );
  }

  listReservationsClicked(): void {
    this.router.navigateByUrl(RESERVATIONS_ROUTE);
  }

  pageClicked(page): void {
    this.contactsProvider.changePage(page);
  }

  createContactClicked(): void {
    this.router.navigateByUrl(CONTACTS_CREATE_ROUTE);
  }

  sort(sortColumn): void{
    const { column } = this.ordering;
    if (column && column.startsWith(sortColumn)){
      return this.contactsProvider.changeOrdering({ column: `-${sortColumn}`});
    }
    this.contactsProvider.changeOrdering({ column: sortColumn });
  }

  getSortClass(sortColumn): string {
    if (!this.ordering) {
      return '';
    }
    const { column } = this.ordering;
    if (column && column.startsWith('-') && column.endsWith(sortColumn)) {
      return 'down-sort';
    }
    else if (column === sortColumn) {
      return 'up-sort';
    }

    return '';
  }

  removeContact(contactId): void {
    this.contactsProvider.removeContact(contactId);
  }

  editContact(contactId): void {
    this.router.navigateByUrl(CONTACTS_EDIT_ROUTE.replace(':id', contactId.toString()));
  }

  showReservationsByContact(contactId): void {
    this.router.navigateByUrl(RESERVATIONS_BY_CONTACT_ROUTE.replace(':id', contactId.toString()));
  }
}
