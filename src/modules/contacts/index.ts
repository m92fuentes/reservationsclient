import { CONTACTS_LIST_ROUTE } from './constants';
import { ContactsListComponent } from './contacts-list/contacts-list.component';

export {
  ContactsListComponent,
  CONTACTS_LIST_ROUTE,
};
