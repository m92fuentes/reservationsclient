import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import {SharedComponentsModule} from '../shared-components/shared-components.module';
import {AngularSvgIconModule} from 'angular-svg-icon';

@NgModule({
  declarations: [ContactsListComponent],
  imports: [
    CommonModule,
    SharedComponentsModule,
    AngularSvgIconModule
  ]
})
export class ContacsModule { }
