import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

type inputType = 'input' | 'select' | 'date';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    },
  ]
})
export class InputComponent implements OnInit, OnChanges, ControlValueAccessor {

  @Input() className = '';
  @Input() type: inputType = 'input';
  @Input() tab;
  @Input() readonly;
  @Input()
  get value(): string {
    return this.pValue;
  }
  set value(value) {
    this.pValue = value;
    this.valueChanged.emit(value);

    if (this.pushChanges) {
      this.pushChanges(value);
    }
  }
  @Output() valueChanged = new EventEmitter();
  @Output() clicked = new EventEmitter();

  optionsOpened = false;
  pValue = null;

  private pushChanges = null;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('value' in changes ){
      this.optionsOpened = false;
    }
  }

  toggleOptions(): void {
    if (this.readonly){
      return;
    }
    this.optionsOpened = !this.optionsOpened;
  }

  showPlaceholder(): boolean {
    return !this.value && typeof this.value !== 'number';
  }

  sendClicked(): void {
    this.toggleOptions();
    this.clicked.emit();
  }
  registerOnTouched(fn: any): void {}
  registerOnChange(fn: any): void {
    this.pushChanges = fn;
  }
  writeValue(val: any): void {
    this.value = val;
  }

}
