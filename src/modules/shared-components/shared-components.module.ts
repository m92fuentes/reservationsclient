import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { SortInputComponent } from './sort-input/sort-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from './input/input.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PaginationComponent } from './pagination/pagination.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ContactTypeComponent } from './contact-type/contact-type.component';
import { DatePickerInputComponent } from './date-picker-input/date-picker-input.component';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';

@NgModule({
  declarations: [
    ButtonComponent,
    SortInputComponent,
    InputComponent,
    PaginationComponent,
    TopBarComponent,
    ContactTypeComponent,
    DatePickerInputComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    DlDateTimeDateModule,
    DlDateTimePickerModule
  ],
  exports: [
    ButtonComponent,
    SortInputComponent,
    PaginationComponent,
    TopBarComponent,
    InputComponent,
    ContactTypeComponent,
    DatePickerInputComponent,
  ]
})
export class SharedComponentsModule { }
