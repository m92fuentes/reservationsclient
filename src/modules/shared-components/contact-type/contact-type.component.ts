import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-contact-type',
  templateUrl: './contact-type.component.html',
  styleUrls: ['./contact-type.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContactTypeComponent),
      multi: true
    },
  ]
})
export class ContactTypeComponent implements OnInit, ControlValueAccessor, AfterViewInit {
  @Input() className = '';
  @Output() selectionChange = new EventEmitter();
  @Input() tab;
  @Input() readonly;
  @ViewChild('options') ref;
  text = null;

  pValue;
  get value(): number {
    return this.pValue;
  }
  set value(v) {
    this.pValue = v;
    this.selectionChange.emit(v);

    if (this.pushChanges){
      this.pushChanges(v);
    }
    this.setSelectedChild();
  }

  private pushChanges = null;
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  clicked(event): void {
    this.text = event.target.innerText;
    const { value } = event.target.attributes;
    this.value = parseInt(value.value, 10);
  }

  ngAfterViewInit(): void {
    this.setSelectedChild();
    this.cdr.detectChanges();
  }

  setSelectedChild(): void {
    if (!this.ref) {
      return;
    }

    const option = Array.from(this.ref.nativeElement.children).find(
      // @ts-ignore
      c => c.attributes.value.value === this.value.toString(),
    );
    if (option) {
      // @ts-ignore
      this.text = option.innerText;
    }
    else {
      this.text = '';
    }
  }

  registerOnTouched(fn: any): void {}
  registerOnChange(fn: any): void {
    this.pushChanges = fn;
  }
  writeValue(val: any): void {
    this.value = val;
  }

}
