import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IPaging } from '../../../types/Requests';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() pagination: IPaging = { page: 2 };
  @Output() pageClicked = new EventEmitter();
  lowPages = [1, 2, 3, 4, 5];
  highPages = [ 10, 20, 30];

  constructor() { }

  ngOnInit(): void {
  }

  click(page): void {
    if (this.pagination.page === page){
      return;
    }
    this.pageClicked.emit(page);
  }
}
