import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-date-picker-input',
  templateUrl: './date-picker-input.component.html',
  styleUrls: ['./date-picker-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerInputComponent),
      multi: true
    },
  ]
})
export class DatePickerInputComponent implements OnInit, ControlValueAccessor {

  @Input() controlName;
  @Input() tab;
  @Input() readonly;

  pSelectedDate;
  openedDateSelector = false;

  private pushChanges = null;

  get selectedDate(): string {
    return this.pSelectedDate;
  }

  set selectedDate(value) {
    if (value !== this.pSelectedDate) {
      this.openedDateSelector = false;
    }
    this.pSelectedDate = value;
    if (this.pushChanges){
      this.pushChanges(value);
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

  openDateSelector(): void {
    if (this.readonly){
      return;
    }
    this.openedDateSelector = true;
  }
  registerOnTouched(fn: any): void {}
  registerOnChange(fn: any): void {
    this.pushChanges = fn;
  }
  writeValue(val: any): void {
    this.selectedDate = val;
  }

}
