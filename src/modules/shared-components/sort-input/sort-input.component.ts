import {Component, OnChanges, OnInit, Output, SimpleChanges, EventEmitter, Input} from '@angular/core';

const columns = [
  'creationTime',
  '-creationTime',
  'name',
  '-name',
  'ranking',
];

@Component({
  selector: 'app-sort-input',
  templateUrl: './sort-input.component.html',
  styleUrls: ['./sort-input.component.scss']
})
export class SortInputComponent implements OnInit {
  @Input() className = '';
  @Output() sortChange = new EventEmitter();
  text = null;
  value = null;

  constructor() { }

  ngOnInit(): void {
  }

  clicked(event): void {
    this.text = event.target.innerText;
    const { value } = event.target.attributes;
    const v = parseInt(value.value, 10) - 1;
    if (this.value === v){
      return;
    }
    this.value = v;
    this.sortChange.emit(columns[v]);
  }
}
