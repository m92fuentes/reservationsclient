import { Component, OnInit } from '@angular/core';
import {IsMobileService} from '../../../app/services/is-mobile.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  isMobile = false;

  constructor(private isMobileService: IsMobileService) {
    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
  }

  ngOnInit(): void {
  }

}
