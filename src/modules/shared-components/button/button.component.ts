import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

export type Size = 'small' | 'normal' | 'big';
export type Color = 'red' | 'gray';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() size: Size = 'normal';
  @Input() color: Color = 'gray';
  @Input() disabled;

  constructor() { }

  ngOnInit(): void {
  }
}
