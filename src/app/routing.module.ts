import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { ReservationListComponent, RESERVATIONS_ROUTE, RESERVATIONS_BY_CONTACT_ROUTE } from '../modules/reservations';
import { ReservationCreationComponent, RESERVATION_EDITION_ROUTE, RESERVATION_CREATION_ROUTE, RESERVATION_CREATION_FROM_CONTACT } from '../modules/reservation-creation';
import { ContactsListComponent, CONTACTS_LIST_ROUTE } from '../modules/contacts';
import { ContactCreateComponent, CONTACTS_CREATE_ROUTE, CONTACTS_EDIT_ROUTE } from '../modules/contacts-create';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: RESERVATIONS_ROUTE, component: ReservationListComponent },
      { path: RESERVATIONS_BY_CONTACT_ROUTE, component: ReservationListComponent },
      { path: RESERVATION_CREATION_FROM_CONTACT, component: ReservationCreationComponent },
      { path: RESERVATION_CREATION_ROUTE, component: ReservationCreationComponent },
      { path: RESERVATION_EDITION_ROUTE, component: ReservationCreationComponent },
      { path: CONTACTS_LIST_ROUTE, component: ContactsListComponent },
      { path: CONTACTS_CREATE_ROUTE, component: ContactCreateComponent },
      { path: CONTACTS_EDIT_ROUTE, component: ContactCreateComponent },
      { path: '', redirectTo: RESERVATIONS_ROUTE, pathMatch: 'full' },
      { path: '**', component: PageNotFoundComponentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
