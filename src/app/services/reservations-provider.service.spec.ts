import { TestBed } from '@angular/core/testing';

import { ReservationsProviderService } from './reservations-provider.service';

describe('ReservationsProviderService', () => {
  let service: ReservationsProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationsProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
