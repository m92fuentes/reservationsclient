import { Injectable } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouteChangeService {

  private subject = new Subject();

  constructor(private router: Router) {
    this.listenToUrlChanges = this.listenToUrlChanges.bind(this);

    router.events.subscribe(
      e => e instanceof NavigationEnd && this.listenToUrlChanges()
    );
    setTimeout(this.listenToUrlChanges, 100);
  }

  private listenToUrlChanges(): void {
    this.subject.next(this.router.url);
  }

  public subscribe(fn): Subscription {
    return this.subject.subscribe(fn);
  }
}
