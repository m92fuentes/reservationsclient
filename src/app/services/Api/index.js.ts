import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IPaginatedRequest, IPaginatedResult } from '../../../types/Requests';
import IReservation from '../../../types/IReservation';
import { Observable } from 'rxjs';
import IContact from '../../../types/IContact';

const httpOptions = {
  observe: 'body' as const,
  responseType: 'json' as const,
};

@Injectable({
  providedIn: 'root',
})
class Api {
  constructor(private http: HttpClient) {
  }

  private getPaginatedParams(paginatedRequest?: IPaginatedRequest): HttpParams {
    let httpParams: HttpParams = null;

    if (paginatedRequest){
      httpParams = new HttpParams();
      if (paginatedRequest.ordering?.column) {
        httpParams = httpParams.set('Ordering.column', paginatedRequest.ordering.column);
      }
      if (paginatedRequest.paging?.page) {
        httpParams = httpParams.set('Paging.Page', paginatedRequest.paging.page.toString());
      }
    }
    return httpParams;
  }


  getReservations(paginatedRequest?: IPaginatedRequest): Observable<IPaginatedResult<IReservation>> {
    const params = this.getPaginatedParams(paginatedRequest);
    return this.http.get<IPaginatedResult<IReservation>>('/api/reservations',
      {
        ...httpOptions,
        params,
      }
    );
  }
  getReservationsByContact(contactId: number, paginatedRequest?: IPaginatedRequest ): Observable<IPaginatedResult<IReservation>> {
    const params = this.getPaginatedParams(paginatedRequest);
    return this.http.get<IPaginatedResult<IReservation>>(`/api/reservations/byContact/${contactId}`,
      {
        ...httpOptions,
        params,
      }
    );
  }

  getReservationById(id: number): Observable<IReservation>{
    return this.http.get<IReservation>(`/api/reservations/${id}`);
  }

  createReservation(reservation: IReservation): Observable<IReservation>{
    return this.http.post<IReservation>('/api/reservations', reservation);
  }

  updateReservation(reservation: IReservation): Observable<IReservation> {
    return this.http.put<IReservation>(`/api/reservations/${reservation.reservationId}`, reservation, httpOptions);
  }

  removeReservation(reservationId: number): Observable<null>{
    return this.http.delete<null>(`/api/reservations/${reservationId}`);
  }

  getContacts(paginatedRequest?: IPaginatedRequest): Observable<IPaginatedResult<IContact>> {
    const params = this.getPaginatedParams(paginatedRequest);
    return this.http.get<IPaginatedResult<IContact>>('/api/contacts', {
      ...httpOptions,
      params,
    });
  }

  removeContact(contactId: number): Observable<null> {
    return this.http.delete<null>(`/api/contacts/${contactId}`);
  }

  getContactById(contactId: number): Observable<IContact> {
    return this.http.get<IContact>(`/api/contacts/${contactId}`);
  }

  getContactByName(name: string): Observable<IContact> {
    let params = new HttpParams();
    params = params.set('name', name);

    return this.http.get<IContact>('/api/contacts/byName', {
      ...httpOptions,
      params
    });
  }

  createContact(contact: IContact): Observable<IContact> {
    return this.http.post<IContact>('/api/contacts', contact);
  }

  updateContact(contact: IContact): Observable<IContact> {
    return this.http.put<IContact>(`/api/contacts/${contact.contactId}`, contact);
  }

}

export default Api;
