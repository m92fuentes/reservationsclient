import { Injectable } from '@angular/core';
import Api from './Api/index.js';
import { Observable, Subject } from 'rxjs';
import { IOrdering, IPaginatedRequest } from '../../types/Requests';
import IReservation from '../../types/IReservation';
import { isOnReservationByContactPath, isOnReservationPath } from '../../shared/helpers';
import {RouteChangeService} from './route-change.service';

@Injectable({
  providedIn: 'root'
})
export class ReservationsProviderService {

  constructor(private api: Api, private routeChangeService: RouteChangeService) {
    this.listenToUrlChanges = this.listenToUrlChanges.bind(this);
    this.subscribeReservationResponse = this.subscribeReservationResponse.bind(this);

    this.routeChangeService.subscribe(this.listenToUrlChanges);
  }

  public paginatedReservationSubject = new Subject();
  public contactNotFoundSubject = new Subject();
  public byContactSubject = new Subject();
  private lastPaginatedRequest: IPaginatedRequest = null;

  private currentContactId;

  private previousSubscription;

  private listenToUrlChanges(url): void{
    if (isOnReservationByContactPath(url)){
      this.currentContactId = +url.split('/').slice(-1)[0];
      this.api.getContactById(this.currentContactId).subscribe(
        c => this.byContactSubject.next(c),
      );
    }
    if (isOnReservationPath(url)) {
      this.currentContactId = null;
      this.contactNotFoundSubject.next(false);
      this.byContactSubject.next(null);
    }
    this.fetchReservations();
  }

  isByContact(): boolean {
    return typeof this.currentContactId === 'number';
  }
  subscribeReservationResponse(obs: Observable<any>): void {
    if (this.previousSubscription) {
      this.previousSubscription.unsubscribe();
    }
    this.previousSubscription = obs.subscribe({
      next: (resp) => {
        this.lastPaginatedRequest = resp;
        this.paginatedReservationSubject.next(resp);
      },
      error: () => this.isByContact() && this.contactNotFoundSubject.next(true)
    });
  }

  fetchReservations(paginatedRequest?: IPaginatedRequest): void {
    if (this.isByContact()){
      return this.subscribeReservationResponse(this.api.getReservationsByContact(this.currentContactId, paginatedRequest));
    }
    this.subscribeReservationResponse(this.api.getReservations(paginatedRequest));
  }

  changePage(page): void {
    this.fetchReservations({
      ordering: this.lastPaginatedRequest.ordering,
      paging: {
        ...this.lastPaginatedRequest.paging,
        page,
      }
    });
  }

  changeOrdering(ordering: IOrdering): void {
    this.fetchReservations({
      ordering,
    });
  }

  updateReservation(reservation: IReservation): void {
    this.api.updateReservation(reservation).subscribe(
      () => this.fetchReservations(this.lastPaginatedRequest),
    );
  }

  removeReservation(reservationId): void {
    this.api.removeReservation(reservationId).subscribe(
      () => this.fetchReservations(this.lastPaginatedRequest),
    );
  }
}
