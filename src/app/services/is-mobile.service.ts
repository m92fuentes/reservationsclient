import { Injectable } from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {mobileMedia} from '../constant';

@Injectable({
  providedIn: 'root'
})
export class IsMobileService {

  constructor(private breakpointObserver: BreakpointObserver) {}

  subscribe(fn): void {
    this.breakpointObserver.observe(mobileMedia).subscribe(
      (m) => fn(m.matches));
  }
}
