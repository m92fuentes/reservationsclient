import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {IOrdering, IPaginatedRequest} from '../../types/Requests';
import Api from './Api/index.js';

@Injectable({
  providedIn: 'root'
})
export class ContactsProviderService {

  public paginatedContactsSubject = new Subject();
  private lastPaginatedRequest: IPaginatedRequest = null;

  constructor(private api: Api) {
  }

  fetchContacts(paginatedRequest?: IPaginatedRequest): void {
    this.api.getContacts(paginatedRequest).subscribe(
      resp => {
        this.lastPaginatedRequest = resp;
        this.paginatedContactsSubject.next(resp);
      }
    );
  }

  changeOrdering(ordering: IOrdering): void {
    this.fetchContacts({
      ordering,
    });
  }

  changePage(page): void {
    this.fetchContacts({
      ordering: this.lastPaginatedRequest.ordering,
      paging: {
        ...this.lastPaginatedRequest.paging,
        page,
      }
    });
  }

  removeContact(contactId): void {
    this.api.removeContact(contactId).subscribe(
      () => this.fetchContacts(this.lastPaginatedRequest),
    );
  }
}
