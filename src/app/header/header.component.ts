import {Component, OnDestroy, OnInit} from '@angular/core';
import { IsMobileService } from '../services/is-mobile.service';
import {RouteChangeService} from '../services/route-change.service';
import {
  isOnContactCreationPath, isOnContactEditionPath,
  isOnContactsPath,
  isOnReservationByContactPath, isOnReservationCreationPath,
  isOnReservationCreationFromContactPath, isOnReservationEditionPath,
  isOnReservationPath, getIdFromUrl
} from '../../shared/helpers';
import {Router} from '@angular/router';
import {RESERVATION_CREATION_FROM_CONTACT, RESERVATION_CREATION_ROUTE} from '../../modules/reservation-creation';
import {CONTACTS_CREATE_ROUTE} from '../../modules/contacts-create';
import {RESERVATIONS_BY_CONTACT_ROUTE, RESERVATIONS_ROUTE} from '../../modules/reservations';
import {CONTACTS_LIST_ROUTE} from '../../modules/contacts';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isMobile = false;
  isReservationsList;
  isReservationCreation;
  isContactList;
  isContactCreation;

  routeChangesSubscription;

  constructor(
    private isMobileService: IsMobileService,
    private routeChanges: RouteChangeService,
    private router: Router,
  ) {
    this.registerUrl = this.registerUrl.bind(this);
  }

  ngOnInit(): void {
    this.isMobileService.subscribe(
      isMobile => this.isMobile = isMobile
    );
    this.routeChangesSubscription = this.routeChanges.subscribe(this.registerUrl);
  }
  ngOnDestroy(): void {
    this.routeChangesSubscription.unsubscribe();
  }

  registerUrl(url): void{
    this.isReservationsList = isOnReservationPath(url) || isOnReservationByContactPath(url);
    this.isContactList = isOnContactsPath(url);
    this.isReservationCreation = isOnReservationCreationFromContactPath(url)
      || isOnReservationCreationPath(url)
      || isOnReservationEditionPath(url);
    this.isContactCreation = isOnContactCreationPath(url) || isOnContactEditionPath(url);
  }

  get showSecondaryButton(): boolean {
    return this.isReservationsList || this.isContactList;
  }

  button1Navigate(): any {
    const { url } = this.router;
    if (this.isReservationsList){
      if (isOnReservationByContactPath(url)){
        return this.router.navigateByUrl(RESERVATION_CREATION_FROM_CONTACT.replace(':id', getIdFromUrl(url)));
      }
      return this.router.navigateByUrl(RESERVATION_CREATION_ROUTE);
    }
    if (this.isContactList) {
      return this.router.navigateByUrl(CONTACTS_CREATE_ROUTE);
    }
    if (this.isReservationCreation) {
      if (isOnReservationCreationFromContactPath(url)){
        return this.router.navigateByUrl(RESERVATIONS_BY_CONTACT_ROUTE.replace(':id', getIdFromUrl(url)));
      }
      return this.router.navigateByUrl(RESERVATIONS_ROUTE);
    }
    if (this.isContactCreation) {
      return this.router.navigateByUrl(CONTACTS_LIST_ROUTE);
    }
  }

  button2Navigate(): any {
    if (this.isReservationsList) {
      return this.router.navigateByUrl(CONTACTS_LIST_ROUTE);
    }
    if (this.isContactList) {
      return this.router.navigateByUrl(RESERVATIONS_ROUTE);
    }
  }
}
