import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { AppRoutingModule } from './routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ReservationsModule } from '../modules/reservations/reservations.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ReservationCreationModule } from '../modules/reservation-creation/reservation-creation.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ContacsModule } from '../modules/contacts/contacs.module';
import { ContactsCreateModule } from '../modules/contacts-create/contacts-create.module';
import { LayoutModule } from '@angular/cdk/layout';
import {HeaderComponent} from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponentComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularSvgIconModule.forRoot(),
    AppRoutingModule,
    ReservationsModule,
    ReservationCreationModule,
    ContacsModule,
    ContactsCreateModule,
    LayoutModule,
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
